# code.gitea.io/gitea-vet v0.2.1
code.gitea.io/gitea-vet
code.gitea.io/gitea-vet/checks
# code.gitea.io/sdk/gitea v0.13.1-0.20210304201955-ff82113459b5
code.gitea.io/sdk/gitea
# gitea.com/noerw/unidiff-comments v0.0.0-20201219085024-64aec5658f2b
gitea.com/noerw/unidiff-comments
gitea.com/noerw/unidiff-comments/types
# github.com/AlecAivazis/survey/v2 v2.2.8
github.com/AlecAivazis/survey/v2
github.com/AlecAivazis/survey/v2/core
github.com/AlecAivazis/survey/v2/terminal
# github.com/Microsoft/go-winio v0.4.16
github.com/Microsoft/go-winio
github.com/Microsoft/go-winio/pkg/guid
# github.com/adrg/xdg v0.3.1
github.com/adrg/xdg
# github.com/alecthomas/chroma v0.8.1
github.com/alecthomas/chroma
github.com/alecthomas/chroma/formatters
github.com/alecthomas/chroma/formatters/html
github.com/alecthomas/chroma/formatters/svg
github.com/alecthomas/chroma/lexers
github.com/alecthomas/chroma/lexers/a
github.com/alecthomas/chroma/lexers/b
github.com/alecthomas/chroma/lexers/c
github.com/alecthomas/chroma/lexers/circular
github.com/alecthomas/chroma/lexers/d
github.com/alecthomas/chroma/lexers/e
github.com/alecthomas/chroma/lexers/f
github.com/alecthomas/chroma/lexers/g
github.com/alecthomas/chroma/lexers/h
github.com/alecthomas/chroma/lexers/i
github.com/alecthomas/chroma/lexers/internal
github.com/alecthomas/chroma/lexers/j
github.com/alecthomas/chroma/lexers/k
github.com/alecthomas/chroma/lexers/l
github.com/alecthomas/chroma/lexers/m
github.com/alecthomas/chroma/lexers/n
github.com/alecthomas/chroma/lexers/o
github.com/alecthomas/chroma/lexers/p
github.com/alecthomas/chroma/lexers/q
github.com/alecthomas/chroma/lexers/r
github.com/alecthomas/chroma/lexers/s
github.com/alecthomas/chroma/lexers/t
github.com/alecthomas/chroma/lexers/v
github.com/alecthomas/chroma/lexers/w
github.com/alecthomas/chroma/lexers/x
github.com/alecthomas/chroma/lexers/y
github.com/alecthomas/chroma/lexers/z
github.com/alecthomas/chroma/quick
github.com/alecthomas/chroma/styles
# github.com/araddon/dateparse v0.0.0-20210207001429-0eec95c9db7e
github.com/araddon/dateparse
# github.com/aymerick/douceur v0.2.0
github.com/aymerick/douceur/css
# github.com/charmbracelet/glamour v0.2.0 => github.com/noerw/glamour v0.2.1-0.20210305125354-f0a29f1de0c2
github.com/charmbracelet/glamour
github.com/charmbracelet/glamour/ansi
# github.com/chris-ramon/douceur v0.2.0
github.com/chris-ramon/douceur/parser
# github.com/cpuguy83/go-md2man/v2 v2.0.0
github.com/cpuguy83/go-md2man/v2/md2man
# github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964
github.com/danwakefield/fnmatch
# github.com/davecgh/go-spew v1.1.1
github.com/davecgh/go-spew/spew
# github.com/dlclark/regexp2 v1.2.0
github.com/dlclark/regexp2
github.com/dlclark/regexp2/syntax
# github.com/emirpasic/gods v1.12.0
github.com/emirpasic/gods/containers
github.com/emirpasic/gods/lists
github.com/emirpasic/gods/lists/arraylist
github.com/emirpasic/gods/trees
github.com/emirpasic/gods/trees/binaryheap
github.com/emirpasic/gods/utils
# github.com/go-git/gcfg v1.5.0
github.com/go-git/gcfg
github.com/go-git/gcfg/scanner
github.com/go-git/gcfg/token
github.com/go-git/gcfg/types
# github.com/go-git/go-billy/v5 v5.0.0
github.com/go-git/go-billy/v5
github.com/go-git/go-billy/v5/helper/chroot
github.com/go-git/go-billy/v5/helper/polyfill
github.com/go-git/go-billy/v5/osfs
github.com/go-git/go-billy/v5/util
# github.com/go-git/go-git/v5 v5.2.0
github.com/go-git/go-git/v5
github.com/go-git/go-git/v5/config
github.com/go-git/go-git/v5/internal/revision
github.com/go-git/go-git/v5/internal/url
github.com/go-git/go-git/v5/plumbing
github.com/go-git/go-git/v5/plumbing/cache
github.com/go-git/go-git/v5/plumbing/color
github.com/go-git/go-git/v5/plumbing/filemode
github.com/go-git/go-git/v5/plumbing/format/config
github.com/go-git/go-git/v5/plumbing/format/diff
github.com/go-git/go-git/v5/plumbing/format/gitignore
github.com/go-git/go-git/v5/plumbing/format/idxfile
github.com/go-git/go-git/v5/plumbing/format/index
github.com/go-git/go-git/v5/plumbing/format/objfile
github.com/go-git/go-git/v5/plumbing/format/packfile
github.com/go-git/go-git/v5/plumbing/format/pktline
github.com/go-git/go-git/v5/plumbing/object
github.com/go-git/go-git/v5/plumbing/protocol/packp
github.com/go-git/go-git/v5/plumbing/protocol/packp/capability
github.com/go-git/go-git/v5/plumbing/protocol/packp/sideband
github.com/go-git/go-git/v5/plumbing/revlist
github.com/go-git/go-git/v5/plumbing/storer
github.com/go-git/go-git/v5/plumbing/transport
github.com/go-git/go-git/v5/plumbing/transport/client
github.com/go-git/go-git/v5/plumbing/transport/file
github.com/go-git/go-git/v5/plumbing/transport/git
github.com/go-git/go-git/v5/plumbing/transport/http
github.com/go-git/go-git/v5/plumbing/transport/internal/common
github.com/go-git/go-git/v5/plumbing/transport/server
github.com/go-git/go-git/v5/plumbing/transport/ssh
github.com/go-git/go-git/v5/storage
github.com/go-git/go-git/v5/storage/filesystem
github.com/go-git/go-git/v5/storage/filesystem/dotgit
github.com/go-git/go-git/v5/storage/memory
github.com/go-git/go-git/v5/utils/binary
github.com/go-git/go-git/v5/utils/diff
github.com/go-git/go-git/v5/utils/ioutil
github.com/go-git/go-git/v5/utils/merkletrie
github.com/go-git/go-git/v5/utils/merkletrie/filesystem
github.com/go-git/go-git/v5/utils/merkletrie/index
github.com/go-git/go-git/v5/utils/merkletrie/internal/frame
github.com/go-git/go-git/v5/utils/merkletrie/noder
# github.com/gorilla/css v1.0.0
github.com/gorilla/css/scanner
# github.com/hashicorp/go-version v1.2.1
github.com/hashicorp/go-version
# github.com/imdario/mergo v0.3.11
github.com/imdario/mergo
# github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99
github.com/jbenet/go-context/io
# github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
github.com/kballard/go-shellquote
# github.com/kevinburke/ssh_config v0.0.0-20201106050909-4977a11b4351
github.com/kevinburke/ssh_config
# github.com/lucasb-eyer/go-colorful v1.2.0
github.com/lucasb-eyer/go-colorful
# github.com/mattn/go-colorable v0.1.8
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.12
github.com/mattn/go-isatty
# github.com/mattn/go-runewidth v0.0.10
github.com/mattn/go-runewidth
# github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d
github.com/mgutz/ansi
# github.com/microcosm-cc/bluemonday v1.0.4
github.com/microcosm-cc/bluemonday
# github.com/mitchellh/go-homedir v1.1.0
github.com/mitchellh/go-homedir
# github.com/muesli/reflow v0.2.0
github.com/muesli/reflow/ansi
github.com/muesli/reflow/indent
github.com/muesli/reflow/padding
github.com/muesli/reflow/wordwrap
# github.com/muesli/termenv v0.7.4
github.com/muesli/termenv
# github.com/olekukonko/tablewriter v0.0.5
github.com/olekukonko/tablewriter
# github.com/pmezard/go-difflib v1.0.0
github.com/pmezard/go-difflib/difflib
# github.com/rivo/uniseg v0.2.0
github.com/rivo/uniseg
# github.com/russross/blackfriday/v2 v2.1.0
github.com/russross/blackfriday/v2
# github.com/sergi/go-diff v1.1.0
github.com/sergi/go-diff/diffmatchpatch
# github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
github.com/skratchdot/open-golang/open
# github.com/stretchr/testify v1.7.0
github.com/stretchr/testify/assert
# github.com/urfave/cli/v2 v2.3.0
github.com/urfave/cli/v2
# github.com/xanzy/ssh-agent v0.3.0
github.com/xanzy/ssh-agent
# github.com/yuin/goldmark v1.3.1
github.com/yuin/goldmark
github.com/yuin/goldmark/ast
github.com/yuin/goldmark/extension
github.com/yuin/goldmark/extension/ast
github.com/yuin/goldmark/parser
github.com/yuin/goldmark/renderer
github.com/yuin/goldmark/renderer/html
github.com/yuin/goldmark/text
github.com/yuin/goldmark/util
# github.com/yuin/goldmark-emoji v1.0.1
github.com/yuin/goldmark-emoji
github.com/yuin/goldmark-emoji/ast
github.com/yuin/goldmark-emoji/definition
# golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
golang.org/x/crypto/blowfish
golang.org/x/crypto/cast5
golang.org/x/crypto/chacha20
golang.org/x/crypto/curve25519
golang.org/x/crypto/ed25519
golang.org/x/crypto/ed25519/internal/edwards25519
golang.org/x/crypto/internal/subtle
golang.org/x/crypto/openpgp
golang.org/x/crypto/openpgp/armor
golang.org/x/crypto/openpgp/elgamal
golang.org/x/crypto/openpgp/errors
golang.org/x/crypto/openpgp/packet
golang.org/x/crypto/openpgp/s2k
golang.org/x/crypto/poly1305
golang.org/x/crypto/ssh
golang.org/x/crypto/ssh/agent
golang.org/x/crypto/ssh/internal/bcrypt_pbkdf
golang.org/x/crypto/ssh/knownhosts
golang.org/x/crypto/ssh/terminal
# golang.org/x/net v0.0.0-20210226172049-e18ecbb05110
golang.org/x/net/context
golang.org/x/net/html
golang.org/x/net/html/atom
golang.org/x/net/internal/socks
golang.org/x/net/proxy
# golang.org/x/sys v0.0.0-20210305034016-7844c3c200c3
golang.org/x/sys/cpu
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/plan9
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/term v0.0.0-20210220032956-6a3ed077a48d
golang.org/x/term
# golang.org/x/text v0.3.5
golang.org/x/text/transform
golang.org/x/text/width
# golang.org/x/tools v0.1.0
golang.org/x/tools/go/analysis
golang.org/x/tools/go/analysis/internal/analysisflags
golang.org/x/tools/go/analysis/internal/facts
golang.org/x/tools/go/analysis/unitchecker
golang.org/x/tools/go/ast/astutil
golang.org/x/tools/go/types/objectpath
golang.org/x/tools/internal/analysisinternal
golang.org/x/tools/internal/lsp/fuzzy
# gopkg.in/warnings.v0 v0.1.2
gopkg.in/warnings.v0
# gopkg.in/yaml.v2 v2.4.0
gopkg.in/yaml.v2
# gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
gopkg.in/yaml.v3
